---
date: "2019-09-04"
draft: false
title: "Sep 28, 2019: Hello Summer"
img: "img/events/hello-summer/sun.svg"
---

Welcome to our event!

Please see flyer below :smile:

{{% center %}}{{< figure src="/img/events/hello-summer/flyer.jpg" >}}{{% /center %}}
